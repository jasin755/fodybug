using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DotVVM.Framework.ViewModel;

namespace easypack4.ViewModels
{
	public class MainPageViewModel : DotvvmViewModelBase
	{

        public string Title { get; set; }

	    public List<State> States { get; set; } = new List<State>()
	    {
	        new State()
	        {
	            Id = 1,
	            Name = "Prdel"
	        },

            new State()
            {
                Id = 2,
                Name = "Kokot"
            }
        };

	    public State SelectedState { get; set; }

	    public MainPageViewModel()
	    {
	        Title = "Test";
	    }

	}

    public class State
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

}
